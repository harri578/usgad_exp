import argparse
import pickle
import torch
import math

from torch import nn
from torch.distributions import constraints
from contextlib import ExitStack


import pyro
import pyro.distributions as dist
import pyro.optim as optim
from pyro import poutine
from pyro.contrib.oed.eig import _eig_from_ape, pce_eig, _ace_eig_loss, _posterior_loss
from pyro.contrib.oed.eig import opt_eig_ape_loss, _vnmc_eig_loss, _ace_eig_loss
from pyro.contrib.util import iter_plates_to_shape, rexpand, rmv

from pyro.util import is_bad
import numpy as np
import matplotlib.pyplot as plt

output_dir = "./run_outputs/gradinfo/"

class TensorLinear(nn.Module):
    __constants__ = ['bias']

    def __init__(self, *shape, bias=True):
        super(TensorLinear, self).__init__()
        self.in_features = shape[-2]
        self.out_features = shape[-1]
        self.batch_dims = shape[:-2]
        self.weight = nn.Parameter(torch.Tensor(*self.batch_dims, self.out_features, self.in_features))
        if bias:
            self.bias = nn.Parameter(torch.Tensor(*self.batch_dims, self.out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.weight, a=math.sqrt(5))
        if self.bias is not None:
            fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.weight)
            bound = 1 / math.sqrt(fan_in)
            nn.init.uniform_(self.bias, -bound, bound)

    def forward(self, input):
        return rmv(self.weight, input) + self.bias


class PosteriorGuide(nn.Module):
    def __init__(self, y_dim, w_dim, batching):
        super(PosteriorGuide, self).__init__()
        n_hidden = 64
        self.linear1 = TensorLinear(*batching, y_dim, n_hidden)
        self.linear2 = TensorLinear(*batching, n_hidden, n_hidden)
        self.output_layer = TensorLinear(*batching, n_hidden, w_dim+w_dim)
        # self.covariance_shape = batching + (w_dim, w_dim)
        self.softplus = nn.Softplus()
        self.relu = nn.ReLU()

    def forward(self, y_dict, design_prototype, observation_labels, target_labels):
        # y = (y_dict["y"] - y_dict["y"].mean()) / torch.sqrt(y_dict["y"].var())
        y = y_dict["y"] - .5
        x = self.relu(self.linear1(y))
        x = self.relu(self.linear2(x))
        final = self.output_layer(x)
        
        posterior_mean = final[..., 0:3]
        posterior_scale = self.softplus(final[...,3:6])

        # import pdb
        # pdb.set_trace()
        # posterior_scale = pyro.param(
        #     "posterior_scale",
        #     0.2236*torch.ones(posterior_mean.shape[-1], device=posterior_mean.device),
        #     constraint=constraints.positive)


        pyro.module("posterior_guide", self)    
        batch_shape = design_prototype.shape[:-1]
        with ExitStack() as stack:
            for plate in iter_plates_to_shape(batch_shape):
                stack.enter_context(plate)
            pyro.sample("w", dist.LogNormal(posterior_mean, posterior_scale).to_event(1))

    def reset_parameters(self):
        self.linear1.reset_parameters()
        self.linear2.reset_parameters()
        self.output_layer.reset_parameters()


def neg_loss(loss):
    def new_loss(*args, **kwargs):
        return (-a for a in loss(*args, **kwargs))

    return new_loss


def make_pk_model(w_loc, w_scale, obs_sd_add, obs_sd_mult, xi_init, observation_label='y'):
    def pk_model(design_prototype):
        design = xi_init.expand(design_prototype.shape)

        batch_shape = design.shape[:-1]
        batch_shape_ = batch_shape[1:]
        D = 400.
        with ExitStack() as stack:
            for plate in iter_plates_to_shape(batch_shape_):
                stack.enter_context(plate)
            w = pyro.sample("w", dist.LogNormal(w_loc, w_scale).to_event(1))
            new_size = torch.ones(len(w.shape))
            new_size[0] = batch_shape[0]
            new_size = tuple(new_size.int().tolist())
            w = w.repeat(new_size)
            w = w.reshape(batch_shape + w.shape[-1:])
            ka, ke, V = w[...,0], w[...,1], w[...,2]
            design_ = design.squeeze(-1)

            ysi = (D / V) * (ka / (ka - ke)) * (torch.exp(-ke*design_) - torch.exp(-ka*design_))
            ysi = ysi.unsqueeze(-1)

            obs_sd = torch.sqrt(ysi.detach()**2*obs_sd_mult**2 + obs_sd_add**2)
            y = pyro.sample(observation_label, dist.Normal(ysi, obs_sd).to_event(1))
            return y

    return pk_model



def main(name, num_inner_samples, num_outer_samples, device, num_parallel):
    xi = args.xi*torch.ones(1).to(device)

    print(xi)
    w_prior_loc = torch.tensor([0.1, np.log(0.1), np.log(20)]).float().to(device)
    w_prior_scale = torch.tensor([0.2236, 0.2236, 0.2236]).to(device)

    obs_sd_add = math.sqrt(0.1)
    obs_sd_mult = math.sqrt(0.01)
    model = make_pk_model(
        w_prior_loc, w_prior_scale, obs_sd_add, obs_sd_mult, xi)

    guide = PosteriorGuide(1, 3, (num_parallel,)).to(device)

    targets = ["w"]
    eig_loss = _ace_eig_loss(model, guide, ["y"], targets)
    loss = neg_loss(eig_loss)
    optimizer = pyro.optim.Adam({"lr": 0.001})

    # Train guide
    print("Training")
    checking = opt_eig_ape_loss(xi, loss, num_samples=(1,10,1,2), num_steps=1000, optim=optimizer, final_num_samples=(10,100,1,1000))
    # Evaluate

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Use ACE/VNMC to evaluate docking designs")
    parser.add_argument("--name", default="", type=str)
    parser.add_argument("--num-inner-samples", default=2500, type=int)
    parser.add_argument("--num-outer-samples", default=100000, type=int)
    parser.add_argument("--num-parallel", default=1, type=int)
    parser.add_argument("--device", default="cuda:0", type=str)
    parser.add_argument("--xi", default=1, type=int)
    args = parser.parse_args()

    main(args.name, args.num_inner_samples, args.num_outer_samples, args.device, args.num_parallel)
